'use strict';

/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

const GLib = imports.gi.GLib;
const Meta = imports.gi.Meta;
const St = imports.gi.St;

let _handles = [];
let _timeouts = [];
let _borders = [];

class FocusIndicator {
  constructor() {
  }

  fade_all_borders() {
    _borders.forEach((_border, index, object)=>{
      if(_border) {
        _border.add_style_class_name('fade');
      }
    });
  }

  remove_all_borders(){
    _borders.forEach((_border, index, object) => {
      if(_border && typeof _border.destroy !== "undefined") {
        _border.destroy();
        object.splice(index, 1);
      }
    });
  }

  remove_all_timeouts(){
    _timeouts.splice(0).forEach((t) => {
      if (t) {
        GLib.Source.remove(t);
        t = null;
      }
    });
  }

  highlight_window(act){
    _timeouts.push(GLib.timeout_add(GLib.PRIORITY_DEFAULT, 1000, () => {
      this.isSizing = false;
      return GLib.SOURCE_CONTINUE;
    }));

    if(this.isSizing) { return; } // TODO: probably this is not needed

    let win = global.display.focus_window;
    if (win == null || win.window_type !== Meta.WindowType.NORMAL) {
      return;
    }

    this.remove_all_borders();
    this.remove_all_timeouts();

    let border = new St.Bin({style_class: "highlight-border"});
    global.window_group.add_child(border);

    let rect = win.get_frame_rect();
    let inset = 10;
    border.set_size(rect.width + (inset * 2), rect.height + (inset * 2));
    border.set_position(rect.x - inset, rect.y - inset);

    border.show();

    _borders.push(border);

    _timeouts.push(GLib.timeout_add(GLib.PRIORITY_DEFAULT, 300, () => {
      this.fade_all_borders();
      return GLib.SOURCE_CONTINUE;
    }));

    _timeouts.push(GLib.timeout_add(GLib.PRIORITY_DEFAULT, 2000, () => {
      this.remove_all_borders();
      return GLib.SOURCE_CONTINUE;
    }));
  }

  update_size(act) {
    if (!_borders.length) {
      return;
    }

    let win = global.display.focus_window;
    if (win == null || win.window_type !== Meta.WindowType.NORMAL) {
      return;
    }

    let border = _borders[0];

    let rect = win.get_frame_rect();
    let inset = 10;
    border.set_size(rect.width + (inset * 2), rect.height + (inset * 2));
    border.set_position(rect.x - inset, rect.y - inset);
  }

  enable() {
    _handles.push(global.display.connect('notify::focus-window', (_, act) => {this.highlight_window(act);}));
    _handles.push(global.window_manager.connect('size-change', () => {this.isSizing = true;}));
    _handles.push(global.window_manager.connect('size-changed', () => {
      this.update_size();
      this.isSizing = false;
    }));
    _handles.push(global.window_manager.connect('unminimize', () => {this.isSizing = true;}));
    _handles.push(global.display.connect('grab-op-begin', () => {this.remove_all_borders();}));
    _handles.push(global.display.connect('grab-op-end', () => {this.remove_all_borders();}));
  }

  disable() {
    _handles.splice(0).forEach(h => global.window_manager.disconnect(h));
    this.remove_all_timeouts();
    this.remove_all_borders();
    this.isSizing = false;
  }
}

// journalctl -f -o cat /usr/bin/gnome-shell
// gnome-extensions enable focused-window@dimanech.gitlab.gnome.org
let _extension;

function init() {
}

function enable() {
  _extension = new FocusIndicator();
  _extension.enable();
}

function disable() {
    _extension.disable();
    _extension = null;
}

