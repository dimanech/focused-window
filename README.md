# Focused window

Highlights focused standard window with outline

![Focusedwindow](https://i.imgur.com/GRLucJ2.mp4)

---

Based on [Highlight Focus](https://github.com/mipmip/gnome-shell-extensions-highlight-focus) extension by 
Pim Snel (mipmip)

## License

GNU GPL
